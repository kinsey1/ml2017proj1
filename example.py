import csv
import random
import os



def load_data(filename, split, traning_set = [], test_set = []):
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        for x in range(len(dataset)-1):
            for y in range(4):
                dataset[x][y] = float(dataset[x][y])
            if random.random() < split:
                training_set.append(dataset[x])
            else:
                test_set.append(dataset[x])
training_set = []
test = []
load_data("iris.csv", .66, training_set, test)

print(repr(len(training_set)))
print(repr(len(test)))