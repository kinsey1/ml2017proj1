import csv
import random as rand
import math
from operator import itemgetter


'''
Machine learning project 1
Kinsey Reeves - 695705
The following is an implementation of k-nearest neighbour 
to create a model to classify abalone age into either young/old
in abalone-2 or very young, middle age, and old in abalone-3.

The function evaluate is where most variables can be modified. e.g. 
distance metrics, voting methods, k, etc. 
'''


EUC = "euc"
MAN = "man"

MAJORITY = "majority"
INV_DIST = "inv_dist"

YOUNG_STR = "young"
OLD_STR = "old"

V_YOUNG_STR = "very-young"
MIDDLE_AGE_STR = "middle-age"


#------DEFAULTS TO BE CHANGED----------

#2 = abolone2, 3 = abalone3
AB_PROBLEM = 2

#the amount of neighbours used
K_VAL = 5

#proportion of data split
DATA_SPLIT = 0.7

#euclidean or manhatten distance
SIM_METRIC = EUC

#MAJORITY or INV_DIST
VOTING_METRIC = INV_DIST

#Male/Female weight ie. 1 maps (M,F) to (0,1). 0.2 would mean (0,0.2)
MALE_FEM_WEIGHT = 0.2

#to normalize all data between 0 and 1
NORMALIZE = True

#YOUNG or OLD
TRUE_POS = YOUNG_STR

'''
appends to the training set and test set two tuples.
each tuple contains two lists. One which is the abalone data and the 
other the classifier. The new data will be normalized and 'M' mapped to 0, 'F' to 1
this however can be changed so that the when a distance measure like euclidean/manhatten
is used male / female weights are not over represented
ab - which problem is being tested abalone-2 or abalone-3
'''
def preprocess_data(filename, ab = AB_PROBLEM, MF_weight = MALE_FEM_WEIGHT):
    with open(filename, 'rb') as csvfile:
        lines = csv.reader(csvfile)
        dataset = list(lines)
        training_data =[]
        training_classes = []
        test_data = []
        test_classes = []
        
        for instance in dataset:
            r = rand.randrange(0,1)
            if(instance[0]=='M'):
                training_data.append([0] + [float(x) for x in instance[1:8]])
            else:
                training_data.append([MF_weight] + [float(x) for x in instance[1:8]])

            if(ab==2):
                if(int(instance[8]) <= 10):
                    training_classes.append(YOUNG_STR)
                else:
                    training_classes.append(OLD_STR)
                
            elif(ab==3):
                
                if (int(instance[8]) <= 8):
                    training_classes.append(V_YOUNG_STR)
                elif (int(instance[8]) <= 10):
                    training_classes.append(MIDDLE_AGE_STR)
                else:
                    training_classes.append(OLD_STR)
            else:
                print("ERROR - input a correct problem")
                exit(1)

            
        training_set = (training_data, training_classes)
        return training_set


'''
normalizes the data between 0 and 1
so that certain attributes are not 
overrepresented
'''
def normalize(data):
    #list of two tuples containing min/max values for each column, 
    #initialized to the first values
    min_maxs = []
    min_maxs.append((0,1))

    for i in range(1,8):
        temp_min = min(x[i] for x in data)
        temp_max = max(x[i] for x in data)
        min_maxs.append((temp_min, temp_max))


    for inst in data:
        for i in range(1,8):
            min_i = min_maxs[i][0]
            max_i = min_maxs[i][1]
            inst[i] = (inst[i] - min_i)/(max_i - min_i)



def compare_instance(instance_a, instance_b, method=EUC):
    '''
    compares two instances and can either return their euclidean
    or manhatten distance. cosine not used as it's not 
    relevant when normalizing data
    '''
    if(method==EUC):
        sum = 0
        for i in range(0, len(instance_a)):
            temp = (instance_a[i] - instance_b[i])**2

            sum += temp
        return math.sqrt(sum)
    elif(method==MAN):
        sum = 0
        for i in range(0, len(instance_a)):
            sum+= math.fabs(instance_a[i] - instance_b[i])
        return sum

    


def split_data(data, threshhold=DATA_SPLIT):

    '''
    splits the data into two. the test and training blocks using the holdout method
    the threshold is the proportion of the data which will be training. default at .66
    ie 2/3 of the data will be training
    returns a tuple containing 4 lists: 
    - training instances,
    - training classes,
    - test instances
    - test classes
    '''
    instances = data[0]
    classes = data[1]
    training_instances = []
    training_classes = []
    test_instances = []
    test_classes = []

    for i in range(0, len(instances)-1):
        r = rand.random()
        if(r>threshhold):
            test_instances.append(instances[i])
            test_classes.append(classes[i])
        else:
            training_instances.append(instances[i])
            training_classes.append(classes[i])

   
    return (training_instances, training_classes, test_instances, test_classes)



def get_neighbours(instance, training_data, k, method = SIM_METRIC):
    '''
    gets the kth closest neighbours to the instance provided from the 
    training data. 
    possible methods:
    'euc' - euclidean distance
    'man' - manhatten distance
    '''
    distances = []
    training_inst = training_data[0]
    training_class = training_data[1]

    if(method == EUC):
        for i in range(len(training_inst)):
            dist = compare_instance(instance, training_inst[i], EUC)
            distances.append((training_class[i], dist))

    elif(method == MAN):
        for i in range(len(training_inst)):
            dist = compare_instance(instance, training_inst[i], MAN)
            distances.append((training_class[i], dist))

    distances.sort(key = lambda x : x[1])
    neighbours = []

    for i in range(0,k):
        neighbours.append(distances[i])
    return neighbours
        


def predict_class_ab3(neighbours, voting=VOTING_METRIC):
    '''
    predicts a class for abalone3
    method can either be 'm' or 'd'
    m = majority class, NOTE K should be set to an odd value to avoid ties
    d = inverse distance weighting 
    '''
    very_young = 0
    middle_age = 0
    old = 0
    #majority weighting
    if(voting==MAJORITY):
        for i in neighbours:
            if (i[0] == V_YOUNG_STR):
                very_young+=1
            elif (i[0] == MIDDLE_AGE_STR):
                middle_age+=1
            else:
                old +=1
    
    #inverse distance weighting
    elif(voting==INV_DIST):
        closest_neighbour = neighbours[0]
        furthest_neighbour = neighbours[len(neighbours)-1]
        #closest neighbour first case
        if(closest_neighbour[0]==V_YOUNG_STR):
            very_young+=1
        elif (closest_neighbour[0]==MIDDLE_AGE_STR):
            middle_age+=1
        else:
            old+=1

        for i in neighbours[1:]:
            weight = (furthest_neighbour[1] - i[1])/(furthest_neighbour[1]-closest_neighbour[1])
            if(i[0]==V_YOUNG_STR):
                very_young+=weight
            elif (i[0]==MIDDLE_AGE_STR):
                middle_age+=weight
            else:
                old+=weight
    if(very_young > old and very_young > middle_age):
        return V_YOUNG_STR
    if(middle_age > old):
        return MIDDLE_AGE_STR
    return OLD_STR




def predict_class_ab2(neighbours,voting = VOTING_METRIC):
    '''
    predicts a class for abalone2
    method can either be 'm' or 'd'
    m = majority class, NOTE K should be set to an odd value to avoid ties
    d = inverse distance weighting 
    '''    

    young = 0
    old = 0
    #each neighbour has equal weight
    if(voting == MAJORITY):
        for i in neighbours:
            if(i[0]==YOUNG_STR):
                young +=1
            else:
                old+=1
    #inverse linear distance
    elif (voting==INV_DIST):
        closest_neighbour = neighbours[0]
        furthest_neighbour = neighbours[len(neighbours)-1]
        #closest neighbour first case
        if(closest_neighbour[0]==YOUNG_STR):
            young+=1
        else:
            old+=1
        for i in neighbours[1:]:
            weight = (furthest_neighbour[1] - i[1])/(furthest_neighbour[1]-closest_neighbour[1])
            if(i[0]==YOUNG_STR):
                young+=weight
            else:
                old+=weight

    if(old>young):
        return OLD_STR
    return YOUNG_STR

                


def evaluate(data, metric = "all", k = K_VAL, norm = True, voting = VOTING_METRIC):
    
    '''
    evaluates the knn model created and returns different 
    metrics to measure it. 
    
    data - the data with removed classes in a two-tuple 
    as described in the spec
    metric - can be:
        all : all metrics in a list
        accuracy
        error
        precision
        recall
    k - value for the neighbours
    norm - whether or not the data is normalized
    voting - voting metric:
        MAJORITY
        INV_DIST

    '''
    if(norm):
        normalize(data[0])


    #splits into test/training
    split = split_data(data)
    
    training_instances = split[0]
    training_classes = split[1]

    test_instances = split[2]
    test_classes = split[3]

    result = []


    if(AB_PROBLEM==2): 
        for i in range(len(test_instances)):
            neigh = get_neighbours(test_instances[i], (training_instances, training_classes) ,k)
            pred = predict_class_ab2(neigh, voting)
            actual = test_classes[i]
            result.append((pred,actual))
    else:
        for i in range(len(test_instances)):
            neigh = get_neighbours(test_instances[i], (training_instances, training_classes) ,k)
            pred = predict_class_ab3(neigh, voting)
            actual = test_classes[i]
            result.append((pred,actual))
    
    total = 0
    correct = 0

    young = 0
    old = 0

    very_young = 0
    middle_age = 0
    old = 0

    true_pos = 0
    false_pos = 0
    false_neg = 0
    true_neg = 0

    #true positive default is young
    if(AB_PROBLEM==2): 
        for i in result:
            #if predicted equals actual
            if i[0]==i[1]:
                correct+=1
                if(i[0]==TRUE_POS):
                    young+=1
                    true_pos+=1
                else:
                    old+=1
                    true_neg+=1
            #if predicted!= actual
            else:
                if(i[0]==TRUE_POS):
                    false_pos+=1
                else:
                    false_neg+=1
            total+=1
        

    
    accuracy = float(correct)/float(total)
    error = 1-accuracy
    precision = float(true_pos)/(float(true_pos)+float(false_neg))
    recall = float(true_pos)/(float(true_pos)+float(true_neg))
    
    if(AB_PROBLEM==2):
        if(metric=="all"):
            return [("accuracy", accuracy), ("error", error), ("precision", precision), ("recall", recall)]
        elif(metric=="accuracy"):
            return accuracy
        elif(metric=="error"):
            return error
        elif(metric=="recall"):
            return recall
        elif(metric=="precision"):
            return precision
    else:
        return accuracy



print(evaluate(preprocess_data('abdata.csv'), "all", 20, True, INV_DIST))


  







